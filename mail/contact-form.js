Vue.component('contact-form', {
    props: ['title'],
    template: `
<div v-if="exibir" class="contact-form">
    <h2>{{ title }}</h2>
    <div v-if="sucesso" class="alert alert-success"><strong>Mensagem enviada!</strong> Em breve entraremos em contato.</div>
    <div v-else>
        <div class="col-lg-6 col-md-6">
            <div class="contact_details">
                <input type="text" id="nome" name="name" placeholder="Seu Nome" v-model="msg.nome" />
                <div id="nome-if" class="invalid-feedback">Informe seu nome.</div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="contact_details"> 
                <input type="text" id="email" name="email" placeholder="Seu E-mail" v-model="msg.email" />
                <div id="email-if" class="invalid-feedback">{{ email_warning }}</div>
            </div>
        </div> 
        <div class="form_textarea_contact">  
            <textarea id="mensagem" name="message"  cols="30" rows="15" placeholder="Digite Aqui sua Mensagem" v-model="msg.mensagem"></textarea>
            <div id="mensagem-if" class="invalid-feedback">Escreva sua mensagem antes de enviar.</div>
        </div>
        <button id="botao-enviar-msg" class="btn-info hvr-bounce-to-right contact_button" type="button" @click="validar()">ENVIAR</button>
    </div>
</div>
    `,
    data: function() {
        return {
            exibir: false,
            sucesso: false,
            msg: {
                nome: '',
                email: '',
                mensagem: ''
            },
            email_warning: ''
        }
    },
    methods: {
        validar: function() {
            if(!this.campos_vazios(this.msg)) {
                this.ocultar_invalid_feedbacks()
                if(this.email_valido(this.msg.email)) {
                    this.enviar()
                } else {
                    var el = document.getElementById('email')
                    el.classList.add('has-error')
                    this.email_warning = 'Digite um e-mail válido.'
                    this.invalid_feedback('block', 'email')
                }
            } else {
                this.email_warning = 'Informa seu e-mail.'
            }
        },
        campos_vazios: function(campos, excecoes = []) {
            var vazios = 0
            for(let campo in campos) {
                // verifica se o campo está na lista de exceções
                if(!excecoes.includes(campo)) {
                    var el = document.getElementById(campo)
                    // se for do tipo SELECT, seleciona o pai
                    if(el.nodeName === 'SELECT') el = el.parentElement
                    if(!campos[campo]) {
                        vazios += 1
                        el.classList.add('has-error')
                        this.invalid_feedback('block', campo)
                    } else {
                        el.classList.remove('has-error')
                        this.invalid_feedback('none', campo)
                    }
                }
            }
            return vazios // retorn quantidade de campos vazios
        },
        email_valido: function(email) {
            var emailFilter = /^[^@]+@[^@.]+\.[^@]*\w\w$/
            var illegalChars = /[\(\)\<\>\,\;\:\\\"\[\]]/
            return (email === "" || !emailFilter.test(email) || email.match(illegalChars)) ? false : true
        },
        invalid_feedback: function(display, field) {
            var el = document.getElementById(field+'-if')
            el.style.display = display
        },
        ocultar_invalid_feedbacks: function() {
            var els = document.getElementsByClassName("invalid-feedback")
            var i
            for (i = 0; i < els.length; i++) els[i].style.display = "none"
        },
        enviar: function() {
            var bt = document.getElementById('botao-enviar-msg')
            bt.classList.add('disabled')

            var dados = new FormData();
            dados.append('nome', this.msg.nome)
            dados.append('email', this.msg.email)
            dados.append('mensagem', this.msg.mensagem)

            var self = this

            axios.post('./mail/', dados).then(function (response) {
                if(response.data.status === 'enviado') {
                    self.sucesso = true
                } else {
                    bt.classList.remove('disabled')
                }
            }).catch(function (error) {
                console.log(error)
            })
        }
    },
    mounted: function() {
        this.exibir = true
    }
})

var CTT = new Vue({
    el: '#contact-form'
})