<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require 'vendor/autoload.php';

if(isset($_POST['email'])) {
    if(enviar_mensagem($_POST['email'])) {
        print(json_encode(['status'=>'enviado']));
    }
}

function enviar_mensagem($email) {

    $from_name = 'BSB Newsletter';
    $from = 'contato@bsbborrachas.com.br';

    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
    
    try {
        //Server settings
        $mail->SMTPDebug = 0;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        $mail->Host = 'smtp.bsbborrachas.com.br';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = $from;                 // SMTP username
        $mail->Password = 'Bsbborrachas_0101';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        //Recipients
        $mail->setFrom($from, $from_name);
        $mail->addAddress($from, $from_name);     // Add a recipient
        //$mail->addAddress('ellen@example.com');               // Name is optional
        //$mail->addReplyTo('info@example.com', 'Information');
        //$mail->addCC('cc@example.com');
        //$mail->addBCC('bcc@example.com');

        //Attachments
        //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'Site: '.$nome.' - '.$email;

        $msg = '<p><b>Solicitação de inscrição em newsletter em '.date("d/m/Y H:i\h", time()).'</b></p>';
        $msg .= '<p>-------------------------------</p>';
        $msg .= '<p><b>E-mail:</b> '.$email.'</p>';
        $msg .= '<p>-------------------------------</p>';

        $mail->Body = utf8_decode($msg);
        //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        if($mail->send()) {
            return true;
        }
    } catch (Exception $e) {
        echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
    }
}