Vue.component('newsletter', {
    props: ['destinatario'],
    template: `
        <div>
            <h2>Assine a news</h2>
            <div class="footer-subscriber">
                <form class="search-form">
                    <span style="color: #FFFFFF">
                    <input id="email-newsletter" type="text" placeholder="Seu e-mail" v-model="email">
                    <button id="bt-enviar-newsletter" type="button" @click="validar()">Enviar</button>
                    </span>
                </form>
                <div id="invalid-newsletter-email">{{ email_warning }}</div>
            </div>
        </div>
    `,
    data: function() {
        return {
            email: '',
            email_warning: 'Informe o e-mail'
        }
    },
    methods: {
        validar: function() {
            if(this.email) {
                if(this.email_valido(this.email)) {
                    this.warning(0,'')
                    this.enviar()
                } else {
                    this.warning(1,'E-mail inválido.')
                }
            } else {
                this.warning(1,'Informe seu e-mail.')
            }
        },
        email_valido: function(email) {
            if(!email) return false
            var emailFilter = /^[^@]+@[^@.]+\.[^@]*\w\w$/
            var illegalChars = /[\(\)\<\>\,\;\:\\\"\[\]]/
            return (email === "" || !emailFilter.test(email) || email.match(illegalChars)) ? false : true
        },
        warning: function(opacity, msg, success = false) {
            this.email_warning = (msg) ? msg : this.email_warning
            var el = document.getElementById('invalid-newsletter-email')
            el.style.opacity = opacity
            if(success) {
                el.style.color = '#0E2'
            }
        },
        enviar: function() {
            var bt = document.getElementById('bt-enviar-newsletter')
            bt.classList.add('disabled')

            var dados = new FormData();
            dados.append('email', this.email)

            var self = this

            axios.post('./newsletter/', dados).then(function (response) {
                if(response.data.status === 'enviado') {
                    self.warning(1,'E-mail enviado!', true)
                } else {
                    bt.classList.remove('disabled')
                }
            }).catch(function (error) {
                console.log(error)
            })
        }
    }
})

var NSL = new Vue({
    el: '#newsletter-app'
})